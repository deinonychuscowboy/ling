#!/usr/bin/env python3
from typing import *
import sys,pathlib,wand.image,wand.drawing,wand.color

def rotate(arr:Tuple[Tuple[int]])->Tuple[Tuple[int]]:
	newarr:List[Tuple[int]]=[]
	for colindex in range(0,len(arr)):
		newrow:List[int]=[]
		for rowindex in range(len(arr)-1,-1,-1):
			newrow.append(arr[rowindex][colindex])
		newarr.append(tuple(newrow))
	return tuple(newarr)

def invert(arr:Tuple[Tuple[int]])->Tuple[Tuple[int]]:
	newarr:List[Tuple[int]]=[]
	for rowindex in range(0,len(arr)):
		newrow:List[int]=[]
		for colindex in range(0,len(arr)):
			newrow.append(1 if arr[rowindex][colindex]==0 else 0)
		newarr.append(tuple(newrow))
	return tuple(newarr)

def mirrorX(arr:Tuple[Tuple[int]])->Tuple[Tuple[int]]:
	newarr:List[Tuple[int]]=[]
	for rowindex in range(len(arr)-1,-1,-1):
		newrow:List[int]=[]
		for colindex in range(0,len(arr)):
			newrow.append(arr[rowindex][colindex])
		newarr.append(tuple(newrow))
	return tuple(newarr)

def mirrorY(arr:Tuple[Tuple[int]])->Tuple[Tuple[int]]:
	newarr:List[Tuple[int]]=[]
	for rowindex in range(0,len(arr)):
		newrow:List[int]=[]
		for colindex in range(len(arr)-1,-1,-1):
			newrow.append(arr[rowindex][colindex])
		newarr.append(tuple(newrow))
	return tuple(newarr)

def sum(arr:Tuple[Tuple[int]])->int:
	sum=0
	for row in arr:
		for col in row:
			sum+=col
	return sum

def generate(cardinality:int,intrep:int)->Tuple[Tuple[int]]:
	binrep=('{0:0'+str(cardinality**2)+'b}').format(intrep)
	testarr:List[Tuple[int]]=[]
	for rowindex in range(0,cardinality):
		testrow:List[int]=[]
		for colindex in range(0,cardinality):
			testrow.append(int(binrep[rowindex*cardinality+colindex]))
		testarr.append(tuple(testrow))
	return tuple(testarr)

def create(cardinality:int,lowerlimit:int,upperlimit:int)->Tuple[Tuple[Tuple[int]]]:
	dataset:List[Tuple[Tuple[int]]]=[]

	for intrep in range(1,2**(cardinality**2-1)):
		testv1=generate(cardinality,intrep)

		if lowerlimit<=sum(testv1)<=upperlimit:
			testv2=rotate(testv1)
			testv3=rotate(testv2)
			testv4=rotate(testv3)
			testv5=invert(testv1)
			testv6=invert(testv2)
			testv7=invert(testv3)
			testv8=invert(testv4)
			testv9=mirrorX(testv1)
			testv10=mirrorX(testv2)
			testv11=mirrorX(testv3)
			testv12=mirrorX(testv4)
			testv13=mirrorX(testv5)
			testv14=mirrorX(testv6)
			testv15=mirrorX(testv7)
			testv16=mirrorX(testv8)
			testv17=mirrorY(testv1)
			testv18=mirrorY(testv2)
			testv19=mirrorY(testv3)
			testv20=mirrorY(testv4)
			testv21=mirrorY(testv5)
			testv22=mirrorY(testv6)
			testv23=mirrorY(testv7)
			testv24=mirrorY(testv8)

			if (
					testv1 not in dataset
				and testv2 not in dataset
				and testv3 not in dataset
				and testv4 not in dataset
				and testv5 not in dataset
				and testv6 not in dataset
				and testv7 not in dataset
				and testv8 not in dataset
				and	testv9 not in dataset
				and testv10 not in dataset
				and testv11 not in dataset
				and testv12 not in dataset
				and testv13 not in dataset
				and testv14 not in dataset
				and testv15 not in dataset
				and testv16 not in dataset
				and	testv17 not in dataset
				and testv18 not in dataset
				and testv19 not in dataset
				and testv20 not in dataset
				and testv21 not in dataset
				and testv22 not in dataset
				and testv23 not in dataset
				and testv24 not in dataset
			):
				dataset.append(testv1)

	return tuple(dataset)

def output(dataset:Tuple[Tuple[Tuple[int]]])->None:
	counter:int=0
	for glyph in dataset:
		counter+=1
		cardinality:int=len(glyph)
		filename:str="output/"+str(counter)
		with wand.image.Image(width=cardinality,height=cardinality,pseudo="canvas:white") as image:
			with wand.drawing.Drawing() as draw:
				draw.fill_color=wand.color.Color("#000000")
				for rowindex in range(0,cardinality):
					for colindex in range(0,cardinality):
						if glyph[rowindex][colindex]==1:
							draw.color(rowindex,colindex,"point")
				draw(image)
			image.save(filename=filename+".bmp")

if __name__=="__main__":
	pathlib.Path("output").mkdir(parents=True,exist_ok=True)
	if len(sys.argv)!=4:
		print("""Usage: lgen.py [cardinality] [lowerlimit] [upperlimit]
	Cardinality indicates the scale of the glyph, e.g. a cardinality of 3 will give you 3x3 pixel bitmaps
	The limits restrict the ratio of marked vs. unmarked pixels.
	A lower limit of 2 will filter out all glyphs generated with only one marked pixel.
	An upper limit of 7 will filter out all glyphs generated with only one unmarked pixel (assuming cardinality 3).""")
	else:
		output(create(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3])))
