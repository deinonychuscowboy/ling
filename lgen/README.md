LGen
======

LGen is a small python utility to help with generating sufficiently unique glyph sets. Glyphs are generated as square bitmaps, and tested for similarity to other glyphs already generated, by rotation, mirroring, and color inversion. The resulting glyphs are guaranteed to be uniquely recognizable in any orientation, from either side, and are not inversions of each other.
