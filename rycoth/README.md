Rycoth
======

Rycoth is a bitmap font providing an alternate orthography for english phonetics with a heavy linear focus. The basic alphabet is written in a cursive-like flowing form that always returns to a baseline, somewhat like a combination of arabic and devanagari orthographic features. Entire words are composed of a single line, excepting markers used for two purposes: indication of vowels, and voicing of consonants. In lowercase, these markers are dots; in uppercase, they are small tick marks. Uppercase letters are more rigid and carefully constructed, but still possible to form without lifting the (metaphorical) pen from the page (again, excepting markers), while lowercase letters are more lax and approximate in shape.

Related consonants also have largely similar letter shapes; for instance, all alveolar sounds (s,t,d,z) are shaped the same, and differentiated by the direction they point and presence/absence of a voicing marker. Further, s, z, and other fricatives generally point downward, while t, d, and all other stops generally point upward. As english is not a phonologically unambiguous language, not everything follows this rule, particularly for "difficult" letters like c and x, but these "difficult" letters also resemble each other, keeping the easily classifiable ones distinct.

Being a bitmap font, Rycoth won't scale well to larger/smaller sizes, and some modern programs may not recognize it. Bitmap fonts are easy to design, but are pretty old technology.
