Language Resources
==================

Assorted language-related randoms I've created over the years, mostly conlang-related.

Executable code (excluding fonts) in this repository is licensed under the GPL v3. Other resources, such as images, documents, and fonts, are licensed under the Creative Commons v4 BY-NC-SA license.
