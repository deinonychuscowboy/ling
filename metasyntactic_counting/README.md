Metasyntactic Counting
======

Counting system based on my own personal "dialect" of metasyntactic variables.

I've used the standard "foo", "bar", "baz", and then the nonstandard "blarg" since learning to program, and at some point later in my career picked up the semi-standard subsequent "quux", "corge", "grault" and "garply". Following those, if I need more, I tend to lapse into mexican food and/or fruit.

It occurred to me that at 8 "true metasyntactic" words, I was only 2 away from a full decimal counting system, so I added the also-known "plugh" and "xyzzy" to handle the basic digits, and then also made use of "thud", "widget"/"gadget"/"fidget", and "fnord" to handle larger orders of magnitude. The resulting system is fairly similar to english counting, with the exception that I chose to make german-like compound words, and omit the special "ten eleven twelve and the -teens" words that english uses in favor of a "onety one" like system.
